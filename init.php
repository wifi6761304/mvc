<?php
// Project root directory
define("APP_ROOT", __DIR__);
require_once APP_ROOT . '/vendor/autoload.php';
\Tracy\Debugger::enable();

// Fatfree Object (f3) instance (Singleton)
$f3 = \Base::instance();

// Load routes from config/routes.ini
$f3->config(APP_ROOT . '/config/routes.ini', true);
$f3->config(APP_ROOT . '/config/app.ini', true);
$f3->config(APP_ROOT . '/config/config.ini', true);

// Start framework after initalization
$f3->run();