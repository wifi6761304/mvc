<?php
namespace Model;
use \DB\SQL;

abstract class Model {
	protected $db;
	protected \Base $f3;

	/**
	 * Connection to database using F3 loaded ini
	 */
	public function __construct()
	{
		// f3 object to get db credentials
		$this->f3 = \Base::instance();
		$this->db = new SQL(
			"mysql:host={$this->f3->get('database.dbAddress')};dbname={$this->f3->get('database.dbName')}",
			$this->f3->get('database.dbUser'),
			$this->f3->get('database.dbPassword')
		);
		// TODO: redirect if there is an error ($f3->reroute)
	}
}