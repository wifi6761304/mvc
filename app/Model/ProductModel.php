<?php
namespace Model;

class ProductModel extends Model {
	/**
	 * Get single product by id
	 *
	 * @param integer $id
	 * @return array
	 */
	public function getProduct(int $id): array
	{
		$data = [];
		// Wir erwarten nur eine Zeile. Darum lesen wir nur den Eintrag 0 aus.
		$data = $this->db->exec("SELECT * FROM products WHERE product_id = $id")[0] ?? [];
		return $data;
	}

	/**
	 * Get all products
	 *
	 * @return array
	 */
	public function getProducts(): array
	{
		return $this->db->exec('SELECT * FROM products ORDER BY name ASC');
	}
}