<?php
namespace Controller;

class HomeController extends Controller {
	/*
		Eine Funktion, die über eine f3 route aufgerufen wird, erhält immer zwei Parameter:
		1. \Base $f3: Das f3 Objekt, das die gesamte Funktionalität des Frameworks zur Verfügung stellt
		2. array $params: Ein Array, das alle Parameter enthält, die in der Route definiert wurden
	*/
	/**
	 * Homepage
	 *
	 * @param \Base $f3
	 * @param array $params
	 * @return void
	 */
	public function index(\Base $f3, array $params) {
		// $f3->set(name, value) erlaubt es uns, den views eine Variable zur Verfügung zu stellen
		$f3->set('pageTitle', 'Home');
		$f3->set('pageHeading', "Welcome");

		$this->setContent('home.html');
		echo $this->render();
	}

	/**
	 * About page
	 *
	 * @param \Base $f3
	 * @param array $params
	 * @return void
	 */
	public function about(\Base $f3, array $params) {
		$f3->set('pageTitle', 'About us');
		$f3->set('pageHeading', 'About our business');

		$this->setContent('about.html');
		echo $this->render();
	}
}