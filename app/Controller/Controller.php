<?php
namespace Controller;

abstract class Controller {
	protected \Base $f3;

	public function __construct(\Base $f3, array $params)
	{
		$this->f3 = \Base::instance();
	}

	/**
	 * Helper, sets content Template
	 *
	 * @param string $content
	 * @return void
	 */
	protected function setContent(string $content)
	{
		$this->f3->set('content', $this->f3->get('contentDir') . $content);
	}

	/**
	 * Render current page
	 *
	 * @return string
	 */
	protected function render(): string
	{
		return \Template::instance()->render($this->f3->get('viewsDir') . 'index.html');
	}
}