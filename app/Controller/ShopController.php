<?php
namespace Controller;
use \Model\ProductModel;

class ShopController extends Controller {
	private ProductModel $pM;

	/**
	 * Auch der Constructor erhält das $f3 Objekt und die $params
	 *
	 * @return void
	 */
	public function __construct(\Base $f3, $params)
	{
		parent::__construct($f3, $params);
		$this->pM = new ProductModel();
	}

	/**
	 * Shop Home
	 *
	 * @param \Base $f3
	 * @param array $params
	 * @return void
	 */
	public function index(\Base $f3, array $params) {
		$f3->set('pageTitle', 'Shop');
		$f3->set('pageHeading', "Shop");

		// get all products
		$products = $this->pM->getProducts();
		$f3->set('products', $products);

		$this->setContent('shop.html');
		echo $this->render();
	}

	/**
	 * Single Product
	 *
	 * @param \Base $f3
	 * @param array $params
	 * @return void
	 */
	public function product(\Base $f3, array $params) {
		$f3->set('pageTitle', 'Shop - Product');
		$f3->set('pageHeading', "Product");
		$f3->set('content', $f3->get('contentDir') . 'product.html');

		// Get product data
		$id = filter_var($params['id'] ?? false, FILTER_VALIDATE_INT) ?: 0;
		// Alternativ: Weiterleitung auf Fehlerseite (siehe $f3->reroute)

		$product = $this->pM->getProduct($id) ?? [];
		$f3->set('product', $product);

		$this->setContent('product.html');
		echo $this->render();
	}
}